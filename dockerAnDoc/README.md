# 星空道极宗的docker，

#### 介绍

道：docker-compose一键安装cicd环境，Docker管理 + api管理 + MySQL 及审计+ Nacos + Redis
道生一，一键部署开发，测试，构建，发布环境

#### 软件架构
![images](image/env.jpg)

Portainer：ip地址:9000
Yearning：ip地址:8000 默认登录账号密码：admin/Yearning_admin
Redis:密码为123456
RabbitMQ管理端：ip地址:15672 登录账号密码：admin/admin
Nacos：ip地址:8848/nacos 登录账号密码默认：nacos/nacos
Sentinel：ip地址:8858 登录账号密码：sentinel/sentinel
Jenkins：ip地址:8080

#### 安装教程

1.  推荐alpine 环境中安装 docker
2.  选择VIRTUAL，36m极速内核、针对虚拟系统进行了优化。
3.  编辑/etc/apk/repositories文件以添加（或取消注释）一行，
更新索引存储库apk update
开始安装apk add docker
要在引导时启动 Docker 守护程序，请运行：rc-update add docker boot
然后手动启动 Docker 守护程序，运行：service docker start

#### 使用说明

新建配置文件
```
$ cp env.example .env
```

开启容器服务
```
$ docker-compose up
```
> 守护进程 `docker-compose up -d`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技
除了all in one 每个可以根据需求独立安装
规则
docker-compose -f docker-compose-XXX.yml -p XXX up -d
例如：
```
docker-compose -f docker-compose-mysql.yml -p mysql up -d
```
