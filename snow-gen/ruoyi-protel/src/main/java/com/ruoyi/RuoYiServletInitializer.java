package com.ruoyi;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * web容器中进行部署
 * /etc/apk/repositories
 *
 * 要在引导时启动 Docker 守护程序，请运行：rc-update add docker boot
 * 然后手动启动 Docker 守护程序，运行：service docker start
 *
 * root@nulige:~# vi /etc/docker/daemon.json
 *
 *
 *{
 *   "registry-mirrors": [
 *     "https://registry.docker-cn.com",
 *     "http://hub-mirror.c.163.com",
 *     "https://docker.mirrors.ustc.edu.cn"
 *   ]
 * }
 * @author ruoyi
 */
public class RuoYiServletInitializer extends SpringBootServletInitializer
{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(GenApplication.class);
    }
}
